import os
import logging
import platform
import time
from pyvirtualdisplay import Display
from selenium import webdriver
from Screenshot import Screenshot_Clipping
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

ob=Screenshot_Clipping.Screenshot()

logging.getLogger().setLevel(logging.INFO)

BASE_URL_HOME = 'https://www.thefryecompany.com'
BASE_URL = 'https://www.thefryecompany.com/womens'
BASE_URL_MEN = 'https://www.thefryecompany.com/mens'
BASE_URL1= '/data/womens.png'



def chrome_example():
    display = Display(visible=0, size=(800, 600))
    display.start()
    logging.info('Initialized virtual display..')

    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument("disable-infobars")
    chrome_options.add_argument("--disable-extensions")  # disabling extensions
    chrome_options.add_argument('--start-maximized')
    chrome_options.add_argument("--disable-notifications")
    chrome_options.add_argument("--disable-dev-shm-usage")
    #chrome_options.add_argument("--verbose")
    #chrome_options.add_argument("--verbose")
    #chrome_options.add_argument("--log-path=/data/chromedriver.log") 

    d = DesiredCapabilities.CHROME
    d['loggingPrefs'] = { 'browser':'ALL' }

    chrome_options.add_experimental_option('prefs', {
        'download.default_directory': os.getcwd(),
        'download.prompt_for_download': False,
    })
    logging.info('Prepared chrome options..')

    browser = webdriver.Chrome(chrome_options=chrome_options,desired_capabilities=d)
    logging.info('Initialized chrome browser..')

    desktop = {'output': str(BASE_URL1) + '-desktop.png',
               'width': 2200,
               'height': 1800}

    browser.get_window_size()
    
    required_width = browser.execute_script('return document.body.parentNode.scrollWidth')
    required_height = browser.execute_script('return document.body.parentNode.scrollHeight')
    #browser.set_window_size(required_width, required_height)
    #browser.set_window_size(required_width, required_height)
    browser.get(BASE_URL)
    time.sleep(5)
    #browser.find_element_by_id('bfx-wm-close-button').click()
    #browser.find_element_by_xpath('//*[@id="bfx-wm-close-button"]').click()
    #browser.find_element_by_xpath('//*[@id="bfx-wm-close-button"]').click()
    #browser.find_element_by_class_name('action-close').click();
    time.sleep(5)
    logging.info('Accessed %s ..', BASE_URL)
    try:
        browser.find_element_by_xpath('//*[@id="bfx-wm-close-button"]').click()

    except NoSuchElementException:
        Q=0

    try:
        browser.find_element_by_xpath('/html/body/div[6]/aside[2]/div[2]/header/button').click()
    except NoSuchElementException:
        Q=0
    #browser.find_element_by_xpath('/html/body/div[6]/aside[2]/div[2]/header/button').click()
    #browser.find_element_by_tag_name('body')#.screenshot(BASE_URL1)
    img_url=ob.Full_Scrrenshot(browser,r'/data/','womens.png')
    logging.info('Womens Category Screenshot Taken %s ..', BASE_URL)
    #browser.set_window_size(original_size['width'], original_size['height'])
    with open("/data/errors.log", "a") as logfile:
        logfile.write('\n')
        logfile.write('""""""""""""""""""""""""WomensPage"""""""""""""""""""""""""')
    for entry in browser.get_log('browser'):
        if entry['level'] != 'INFO':
            with open("/data/errors.log", "a") as logfile:
                logfile.write('\n')
                logfile.write(entry['message'])
        #logging.info('Hompage Screenshot Taken %s ..', entry)



    browser.get(BASE_URL_HOME)
    time.sleep(5)
    img_url=ob.Full_Scrrenshot(browser,r'/data/','home.png')

    #('Accessed %s ..', BASE_URL_HOME)
    logging.info('Hompage Screenshot Taken %s ..', BASE_URL_HOME)

    with open("/data/errors.log", "a") as logfile:
        logfile.write('\n')
        logfile.write('""""""""""""""""""""""""HomePage"""""""""""""""""""""""""')
    for entry in browser.get_log('browser'):
        if entry['level'] != 'INFO':
            with open("/data/errors.log", "a") as logfile:
                logfile.write('\n')
                logfile.write(entry['message'])
        #logging.info('Hompage Screenshot Taken %s ..', entry)


    browser.get(BASE_URL_MEN)
    time.sleep(5)
    img_url=ob.Full_Scrrenshot(browser,r'/data/','men.png')

    #('Accessed %s ..', BASE_URL_HOME)
    logging.info('Hompage Screenshot Taken %s ..', BASE_URL_MEN)

    logging.info('Page title: %s', browser.title)

    with open("/data/errors.log", "a") as logfile:
        logfile.write('\n')
        logfile.write('""""""""""""""""""""""""MensPage"""""""""""""""""""""""""')
    for entry in browser.get_log('browser'):
        if entry['level'] != 'INFO':
            with open("/data/errors.log", "a") as logfile:
                logfile.write('\n')
                logfile.write(entry['message'])
        #logging.info('Hompage Screenshot Taken %s ..', entry)

    browser.quit()
    display.stop()



if __name__ == '__main__':
    chrome_example()

